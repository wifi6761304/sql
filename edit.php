<?php
require_once "init.php";

// Das Formular kann über Link (Get) aufgerufen werden, aber auch über POST, wenn es abgeschickt wurde.
$cid = $_GET['cid'] ?? $_POST['cid'] ?? 0;
if (!filter_var($cid, FILTER_VALIDATE_INT)) die();
require_once "update-customer.php";
?>
<!DOCTYPE html>
<html lang="de">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Edit Customers</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
</head>

<body>
	<div class="container">
		<header>
			<h1>Edit Customer</h1>
		</header>
		<main>
			<h2>Customer</h2>
			<?php
			// $sql = "SELECT * FROM customers WHERE customer_id = $cid";
			// $row = $pdo->query($sql)->fetch(PDO::FETCH_ASSOC);
			/*
				prepared statements: https://www.php.net/manual/en/pdo.prepare.php

				Parameter in prepared statements können als ? angegeben werden. Dann wird
				in execute ein array übergeben, das die Werte in der selben Reihenfolge,
				wie die Fragezeichen im Statement auftauchen, übergeben muss.
				SELECT * FROM customers WHERE customer_id = ?
				$stmt->execute([$cid])

				Es können Parameter auch benannt werden. Im Statement werden als Platzhalter :name eingefügt.
				Execute erhält nun ein assoziatives Array, bei dem der Key der Platzhalter Name ohne : ist.
				SELECT * FROM customers WHERE customer_id = :cid
				$stmt->execute(['cid' => $cid]);

				Prepared Statements sind aus Sicherheitsgründen BEST PRACTICE.
			*/
			// Statement vorbereiten
			$sql = "SELECT * FROM customers WHERE customer_id = :cid";
			// Wir geben der DB ein sql Statement mit Platzhaltern bekannt
			$stmt = $pdo->prepare($sql);
			// Statement ausführen - ähnlich wie $dbo->query
			$stmt->execute(['cid' => $cid]);
			// Result über das Statement holen
			$row = $stmt->fetch();

			if (!empty($row)) : ?>
				<!-- Formular mit textfeldern, die die gerade ausgelesenen Werte enthalten. -->
				<form action="" method="post">
					<input type="hidden" name="cid" value="<?= $row['customer_id'] ?>">
					<div class="mb-3">
						<label for="vorName" class="form-label">Vorname</label>
						<input type="text" name="vorname" class="form-control" id="vorName" value="<?= $row['vorname'] ?>">
					</div>
					<div class="mb-3">
						<label for="nachName" class="form-label">Nachname</label>
						<input type="text" name="nachname" class="form-control" id="nachName" value="<?= $row['nachname'] ?>">
					</div>
					<div class="mb-3">
						<label for="email" class="form-label">E-mail</label>
						<input type="email" name="email" class="form-control" id="email" value="<?= $row['email'] ?>">
					</div>
					<div class="mb-3">
						<button class="btn btn-primary">Update</button>
					</div>
				</form>
			<?php else: ?>
				<p class="alert alert-warning">Keine Daten für diese Kundennummer vorhanden!</p>
			<?php endif; ?>
		</main>
	</div>
</body>

</html>