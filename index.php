<?php require_once "init.php"; ?>
<!DOCTYPE html>
<html lang="de">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Customers Startseite</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
</head>

<body>
	<div class="container">
		<header>
			<h1>Customers Startseite</h1>
		</header>
		<main>
			<h2>Customers</h2>
			<table class="table table-striped">
				<thead>
					<tr>
						<th>ID</th>
						<th>Vorname</th>
						<th>Nachname</th>
						<th>E-Mail</th>
						<th>Edit</th>
					</tr>
				</thead>
				<tbody>
					<!-- Alle Customers ausgeben -->
					<?php
					$sql = "SELECT * FROM customers";
					foreach ($pdo->query($sql)->fetchAll(PDO::FETCH_ASSOC) as $row) {
						echo <<<TR
	<tr>
		<td>{$row['customer_id']}</td>
		<td>{$row['vorname']}</td>
		<td>{$row['nachname']}</td>
		<td>{$row['email']}</td>
		<td>
		    <a class="btn btn-success" href="edit.php?cid={$row['customer_id']}">Edit</a>
		</td>
	</tr>
TR;
					}
					?>
				</tbody>
			</table>
		</main>
	</div>
</body>

</html>