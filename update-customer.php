<?php
require_once "init.php";
if(!empty($_POST)) {
	// id, vorname, nachname, email
	$cid = trim($_POST['cid'] ?? '');
	$vorname = trim($_POST['vorname'] ?? '');
	$nachname = trim($_POST['nachname'] ?? '');
	$email = trim($_POST['email'] ?? '');

	// TODO: Validierung required
	// TODO: neuen Datensatz anlegen
	// TODO: Messages im Fehlerfall ausgeben

	if ($cid !== '') {
		// Update in Db
		// $sql = "UPDATE customers SET vorname='$vorname', nachname='$nachname', email='$email' WHERE customer_id = $cid";
		// $res = $pdo->query($sql);
		$sql = "UPDATE customers SET vorname= :vorname, nachname= :nachname, email= :email WHERE customer_id = :cid";
		$stmt = $pdo->prepare($sql);
		$stmt->execute([
			'email' => $email,
			'nachname' => $nachname,
			'vorname' => $vorname,
			'cid' => $cid
		]);
	}
}