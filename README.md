# sql

Wir lernen mit SQL und mySQL/mariaDB umzugehen.

## SQL SELECT

### Basics
- [mysql.org tutorial](https://www.mysqltutorial.org/mysql-basics/mysql-select-from/)
- [mysql official](https://dev.mysql.com/doc/)

`SELECT select_list FROM tablename`

**SELECT was FROM woher**

Das Ergebnis eines Select nennt man result oder result set.

\* ist der Universalselektor, dh. alles (Joker)

Beispiele:
`SELECT * FROM customers` - selektiert alles in der Tabelle customers

`SELECT email FROM customers` - selektiert die Spalte email aus der Tabelle customers

`SELECT email, nachname FROM customers` - selektiert email und Nachname aus der Tabelle customers

Wir können auch bei der Angabe der Spaltennamen den Tabellennamen durch Punkt getrennt voranstellen:

`SELECT customers.email, customers.nachname FROM customers` - ident mit vorigem Select.

**bei Queries über mehrere Tabellen wird der Spaltenname vorangestellt**

### Die Where Klausel

Mit Hilfe von `WHERE` können wir das Ergebnis einschränken. WHERE beschreibt ähnlich dem if in PHP eine Bedingung.

`SELECT * FROM customers WHERE customer_id = 12` Selektiert alle Spalten die die customer_id 12 haben. Da die customer_id unique unser Primärschlüssel ist, erhalten wir bestenfalls eine Zeile. Oder ein leeres Resultset, wenn es keinen Eintrag mit customer_id 12 gibt.

### Sortieren

Mit Hilfe von `ORDER BY` können wir die Ergebnisse nach vorgegebenen Kriterien sortieren lassen. ASC = ascending, ausfteigend. DESC = descending, absteigend.

`SELECT * FROM `customers` ORDER BY nachname ASC, vorname ASC` selektiert alle Spalten und sortiert nach Nachname aufsteigend. Falls unter dem selben Nachnamen mehrere Einträge gefunden werden, werde diese wiederum nach Vorname aufsteigend sortiert.

### LIMIT

Wir können die Anzahl der Datensätze im Resultset beschränken

`SELECT select_list FROM table_name LIMIT [offset,] row_count`

`SELECT * FROM customers ORDER BY nachname ASC LIMIT 30, 10`

## SQL INSERT

Neue Einträge in Tabellen vornehmen

`INSERT INTO table_name(column1, column2,...)
VALUES (value1, value2,...)`

Wir schreiben in die Tabelle table_name in die Spalten in der ersten Klammer die Werte in der zweiten Klammer. Die Wert müssen  der Reihenfolge der definierten Spalten in der ersten Klammer entsprechen.

Es können im selben `INSERT` statement beliebig viele values (in Klammern) eingetragen werden. Diese werden mit Beistrich getrennt in Klammern nach `VALUES` angeführt:

`INSERT INTO table_name(column1, column2,...)
VALUES (value1, value2,...), (value1, value2, ...), (value1, value2, ...)`

Ist der Primary Key auf auto-increment gesetzt, wird diese Spalte ignoriert oder mit dem Wert NULL beschrieben.

`INSERT INTO customers (vorname, nachname, email) VALUES ('Pipi', 'Langstrumpf', 'pl@taktuka.com'), ('Harry', 'Potter', 'potter@howarts.co.uk')`

## SQL UPDATE

Wir aktualisieren bereits vorhandene Datensätze.

**WICHTIG:** wenn keine oder eine unsaubere WHERE Klausel angegeben wird, werden potentiell Datensätze aktualisiert, die eigentlich  nicht aktualisiert werden sollten -> compromised database.

`UPDATE table_name SET column1 = value, column2 = value WHERE id = 12`

`UPDATE customers SET nachname = "Müller" WHERE nachname = "Bennitt"`

## SQL DELETE

Löscht Datensätze

`DELETE FROM table_name WHERE condition`

**WICHTIG:** Delete funktioniert auch ohne WHERE bzw. achte gut auf
die Condition. Gelöschte Daten sind für immer verloren!

`DELETE FROM customer WHERE customer_id = 7`